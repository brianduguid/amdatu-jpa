/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.database.schemamigration.demo;

import java.util.Properties;

import javax.sql.DataSource;

import org.amdatu.database.schemamigration.SchemaMigrationService;
import org.osgi.framework.BundleContext;

public class MigrationDemo {

	private volatile DataSource m_ds;
	private volatile SchemaMigrationService m_migrationService;
	private volatile BundleContext m_bundleContext;

	public void start() {
		
		Properties props = new Properties();
		props.setProperty("flyway.initOnMigrate", Boolean.TRUE.toString());
		props.setProperty("flyway.outOfOrder", Boolean.TRUE.toString());
		props.setProperty("flyway.validateOnMigrate", Boolean.TRUE.toString());
		props.setProperty("flyway.tableName", "schema_version_amdatu_demo");
		props.setProperty("flyway.schemas", "amdatu");
		props.setProperty("flyway.initVersion", "0");
		
		
		m_migrationService.migrate(m_ds, m_bundleContext.getBundle(), "migrations", props);

		System.out.println("Migration completed!");
	}
}
