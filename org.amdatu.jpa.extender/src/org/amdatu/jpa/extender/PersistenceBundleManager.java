/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.extender;

import static org.osgi.service.log.LogService.LOG_DEBUG;
import static org.osgi.service.log.LogService.LOG_ERROR;
import static org.osgi.service.log.LogService.LOG_INFO;
import static org.osgi.service.log.LogService.LOG_WARNING;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.persistence.spi.PersistenceProvider;
import javax.sql.DataSource;

import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Bundle;
import org.osgi.service.log.LogService;

public class PersistenceBundleManager {

    private static final String DEFAULT_PERSISTENCE_XML_RESOURCE = "META-INF/persistence.xml";

    private static final String META_PERSISTENCE_HEADER = "Meta-Persistence";

    private volatile LogService m_log;

    private volatile Map<Bundle, List<Component>> m_persistenceUnitInfoComponents = new ConcurrentHashMap<>();

    @SuppressWarnings("unused" /* dependency manager callback */)
    private void addPersistenceBundle(Bundle bundle) {
        synchronized (m_persistenceUnitInfoComponents) {

            if (m_persistenceUnitInfoComponents.containsKey(bundle)) {
                m_log.log(
                    LOG_WARNING,
                    String.format("Trying to add registered bundle [%s] %s", bundle.getBundleId(),
                        bundle.getSymbolicName()));
                return;
            }

            m_log.log(LOG_INFO, "Adding persistence bundle: " + bundle);

            List<Component> persistenceUnitInfoComponents = new ArrayList<Component>();
            DependencyManager dependencyManager = new DependencyManager(bundle.getBundleContext());
            try {
                for (PersistenceDescriptor persistenceDescriptor : readPersistenceDescriptors(bundle)) {

                    PersistenceUnitInfoImpl puInfo = new PersistenceUnitInfoImpl(bundle, persistenceDescriptor);

                    Component puInfoComponent = dependencyManager.createComponent().setImplementation(puInfo);

                    String filter = null;
                    String provider = persistenceDescriptor.getProvider();
                    if (provider != null && provider.trim().length() > 0) {
                        filter = String.format("(javax.persistence.provider=%s)", provider);
                    }

                    puInfoComponent.add(dependencyManager.createServiceDependency()
                        .setService(PersistenceProvider.class, filter)
                        .setCallbacks("addPersistenceProvider", "removePersistenceProvider")
                        .setRequired(true));

                    if (puInfo.getPersistenceDescriptor().getNonJtaDataSource() != null) {
                        String nonJtaDataSource = puInfo.getPersistenceDescriptor().getNonJtaDataSource();
                        String dataSourceFilter = null;
                        if (nonJtaDataSource.split("/").length > 1) {
                            dataSourceFilter = nonJtaDataSource.split("/")[nonJtaDataSource.split("/").length - 1];
                        }

                        puInfoComponent.add(dependencyManager.createServiceDependency()
                            .setService(DataSource.class, dataSourceFilter)
                            .setCallbacks("setNonJtaDataSource", "removeNonJtaDataSource")
                            .setRequired(true));
                    }

                    if (puInfo.getPersistenceDescriptor().getJtaDataSource() != null) {
                        String jtaDataSource = puInfo.getPersistenceDescriptor().getJtaDataSource();
                        String jtaDataSourceFilter = null;
                        if (jtaDataSource.split("/").length > 1) {
                            jtaDataSourceFilter = jtaDataSource.split("/")[jtaDataSource.split("/").length - 1];
                        }

                        puInfoComponent
                            .add(dependencyManager.createServiceDependency()
                                .setService(DataSource.class, jtaDataSourceFilter)
                                .setCallbacks("setJtaDataSource", "removeJtaDataSource")
                                .setRequired(true));
                    }
                    persistenceUnitInfoComponents.add(puInfoComponent);

                    m_persistenceUnitInfoComponents.put(bundle, persistenceUnitInfoComponents);
                    for (Component component : persistenceUnitInfoComponents) {
                        dependencyManager.add(component);
                    }
                }

                m_log.log(LOG_INFO, "New persistence bundle added: " + bundle);
            }
            catch (Exception e) {
                m_log.log(LOG_ERROR, "Failed to register persistence bundle", e);
            }
        }
    }

    @SuppressWarnings("unused" /* dependency manager callback */)
    private void removePersistenceBundle(Bundle bundle) {
        m_log.log(LOG_INFO, "Removing persistence bundle: " + bundle);
        synchronized (m_persistenceUnitInfoComponents) {
            if (m_persistenceUnitInfoComponents.containsKey(bundle)) {
                List<Component> list = m_persistenceUnitInfoComponents.remove(bundle);
                if (list != null) {
                    for (Component component : list) {
                        component.getDependencyManager().remove(component);
                    }
                }
            }
        }
    }

    private List<PersistenceDescriptor> readPersistenceDescriptors(Bundle bundle) {
        PersistenceXmlParser persistenceXmlParser = new PersistenceXmlParser();

        String metaPersistenceHeader = bundle.getHeaders().get(META_PERSISTENCE_HEADER);
        List<String> persistenceXmlFiles = new ArrayList<>();
        persistenceXmlFiles.addAll(Arrays.asList(metaPersistenceHeader.split(",")));
        if (!persistenceXmlFiles.contains(DEFAULT_PERSISTENCE_XML_RESOURCE)) {
            // OSGi JPA specification states that "META-INF/persistence.xml" is
            // always used even if its not in the Meta-Persistence header
            persistenceXmlFiles.add(DEFAULT_PERSISTENCE_XML_RESOURCE);
        }

        List<PersistenceDescriptor> descriptors = new ArrayList<>();
        for (String persistenceXmlFile : persistenceXmlFiles) {
            URL resource = bundle.getResource(persistenceXmlFile);
            if (resource != null) {
                try (InputStream inputStream = resource.openStream()) {
                    descriptors.addAll(persistenceXmlParser.parse(inputStream));
                }
                catch (IOException e) {
                    m_log.log(LOG_ERROR,
                        String.format("Unable to read persistence descriptor [%s].", persistenceXmlFile, e));
                }
            }
            else if (persistenceXmlFile.equals(DEFAULT_PERSISTENCE_XML_RESOURCE) && persistenceXmlFiles.size() > 1) {
                m_log.log(LOG_DEBUG, String.format("Default persistence descriptor [%s] not found.",
                    DEFAULT_PERSISTENCE_XML_RESOURCE));
            }
            else {
                m_log.log(LOG_WARNING, String.format("Persistence descriptor [%s] not found.", persistenceXmlFile));
            }
        }
        return descriptors;
    }
}
