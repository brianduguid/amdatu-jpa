/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jta.manager;

import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.transaction.TransactionManager;

import org.amdatu.jta.ManagedTransactional;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Constants;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

public class TransactionalAspectManager {

	private volatile DependencyManager dependencyManager;

	private volatile Map<ServiceReference<?>, Component> map = new ConcurrentHashMap<>();

	@SuppressWarnings("unused" /* dependency manager callback */)
	private void onAdded(ServiceReference<?> ref, Object service) {
		try {

			Properties properties = new Properties();

			for (String key : ref.getPropertyKeys()) {
				if (Constants.OBJECTCLASS.equals(key)) {
					continue;
				}

				if ("transactional".equals(key)) {
					continue;
				}
				
				Object value = ref.getProperty(key);
				properties.put(key, value);
			}

			properties.put("isManaged", true);
			
			String[] interfaceClass = null;
			
			Class<?>[] interfaces = service.getClass().getInterfaces();
			Set<Class<?>> interfacesSet = new HashSet<>(Arrays.asList(interfaces));
			
			if(interfacesSet.contains(ManagedTransactional.class)) {
				Class<?>[] managedInterfaces = ((ManagedTransactional)service).getManagedInterfaces();
				
				interfaceClass = new String[managedInterfaces.length];
				for (int i = 0; i < managedInterfaces.length; i++){
					interfaceClass[i] = managedInterfaces[i].getName(); 
				}
			} else {
				Object property = ref.getProperty("transactional");
				if (property instanceof String){
					interfaceClass = new String[]{(String) property};
				}else if (property instanceof String[] ){
					interfaceClass = (String[]) property;
				}
				
			}
			
			if(interfaceClass == null) {
				throw new RuntimeException("Invalid managed transaction component registration, no interface set for service [id=" 
						+ ref.getProperty(Constants.SERVICE_ID) + " class=" + service.getClass() +"]" );
			}
			
			Class<?> serviceClass = service.getClass();

			Object ta = Proxy.newProxyInstance(serviceClass.getClassLoader(), serviceClass.getInterfaces(),
					new TransactionalAspect(service));
			
			Component component = dependencyManager
					.createComponent()
					.setInterface(interfaceClass, properties)
					.setImplementation(ta)
					.add(dependencyManager.createServiceDependency().setService(LogService.class).setRequired(false))
					.add(dependencyManager.createServiceDependency().setService(TransactionManager.class)
							.setRequired(true));

			map.put(ref, component);

			dependencyManager.add(component);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	@SuppressWarnings("unused" /* dependency manager callback */)
	private void onRemoved(ServiceReference<?> ref, Object service) {
		Component component = map.remove(ref);
		if (component != null) {
			dependencyManager.remove(component);
		}
	}

}
